//
//  KeyboardAvoidingTrait.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 25/11/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

import UIKit

private struct Constants {
    static var buttonHandlerKey: UInt8 = 100
}

public typealias OnOffsetChanged = ((CGFloat, TimeInterval) -> Void)

public protocol KeyboardAvoidingTrait {}

public extension KeyboardAvoidingTrait {
    func registerForKeyboardAvoiding(observingView: UIView,
                                     onOffsetChanged: OnOffsetChanged? = nil) {
        let keyboardPresentationHandler = KeyboardPresentationHandler(observingView: observingView,
                                                                      onOffsetChanged: onOffsetChanged)
        
        NotificationCenter.default.addObserver(
            keyboardPresentationHandler,
            selector: #selector(KeyboardPresentationHandler.handleKeyboardFrameChanged),
            name: UIResponder.keyboardWillChangeFrameNotification,
            object: nil)
        
        // Store for persistence
        objc_setAssociatedObject(self,
                                 &Constants.buttonHandlerKey,
                                 keyboardPresentationHandler,
                                 objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        
        // Ensure the background colors sync
        observingView.superview?.backgroundColor = observingView.backgroundColor
    }
}

private class KeyboardPresentationHandler {
    private weak var observingView: UIView?
    private let onOffsetChanged: OnOffsetChanged?
    
    init(observingView: UIView,
         onOffsetChanged: OnOffsetChanged?) {
        self.observingView = observingView
        self.onOffsetChanged = onOffsetChanged
    }
    
    @objc func handleKeyboardFrameChanged(notification: Notification) {
        guard
            let userInfo = notification.userInfo,
            let firstResponderView = observingView?.currentFirstResponder,
            let keyboardEndFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
            let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else { return }
        let offsetDelta = UIScreen.main.bounds.size.height - keyboardEndFrame.origin.y
        
        // Don't shift when the element is not behind the keyboard
        if offsetDelta != 0 {
            var translatedCenter = firstResponderView.convert(firstResponderView.center,
                                                              to: nil)
            translatedCenter.y += firstResponderView.frame.size.height/2
            guard translatedCenter.y > keyboardEndFrame.origin.y else { return }
        }
        
        // Shift the view
        updateViewOffset(offset: -offsetDelta,
                         animationDuration: animationDuration)
    }
    
    private func updateViewOffset(offset: CGFloat,
                                  animationDuration: Double) {
        guard let observingView = observingView else { return }
        UIView.animate(withDuration: animationDuration,
                       animations: {
                        var curFrame = observingView.frame
                        curFrame.origin.y = offset
                        observingView.frame = curFrame
        })
        
        // Inform the implementing class of this occurance
        onOffsetChanged?(offset, animationDuration)
    }
}
