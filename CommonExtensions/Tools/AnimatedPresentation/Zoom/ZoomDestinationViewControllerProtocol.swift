//
//  ZoomDestinationViewControllerProtocol.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 25/11/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

public protocol AnimationDestinationViewControllerProtocol {
    var animationAnimator: AnimationAnimator? { get set }
}
