//
//  FadePresentAnimator.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 25/11/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

public final class FadePresentAnimator: AnimationAnimator {
    private let animationSpeed: TimeInterval
    public required init(animationSpeed: TimeInterval) {
        self.animationSpeed = animationSpeed
    }
    
    public override func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return animationSpeed
    }
    
    public override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = transitionContext.viewController(forKey: .from),
            let toViewController = transitionContext.viewController(forKey: .to) else {
                transitionContext.completeTransition(true)
                return
        }
        
        // Add the destination when presenting
        if presenting {
            transitionContext.containerView.addSubview(toViewController.view)
        }

        // Hide the destination
        if presenting {
            toViewController.view.alpha = 0
        }
        
        let duration = transitionDuration(using: transitionContext)
        UIView.animateKeyframes(withDuration: duration,
                                delay: 0,
                                options: UIView.KeyframeAnimationOptions.calculationModeCubic,
                                animations:
            {
                if !self.presenting {
                    // Hide the background over time
                    UIView.addKeyframe(withRelativeStartTime: 0,
                                       relativeDuration: 1,
                                       animations:
                        {
                            fromViewController.view.alpha = 0
                    })
                }
                
                // Center the mimic
                UIView.addKeyframe(withRelativeStartTime: 0,
                                   relativeDuration: 1,
                                   animations:
                    {
                        toViewController.view.alpha = 1
                })
        },
                                completion:
            { _ in
                transitionContext.completeTransition(true)
        })
    }
}
