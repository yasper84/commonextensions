//
//  UITableView+Reusables.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 17/06/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension UITableView {
    public func registerCell<T: UITableViewCell>(type: T.Type) {
        let identifier = String(describing: type)
        let bundle = Bundle(for: type)
        if bundle.path(forResource: identifier, ofType: "nib") != nil {
            register(UINib(nibName: identifier, bundle: bundle),
                     forCellReuseIdentifier: identifier)
        } else {
            register(type,
                     forCellReuseIdentifier: identifier)
        }
    }
}
