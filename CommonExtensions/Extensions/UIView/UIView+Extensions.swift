//
//  UIView+Extensions.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension UIView {
    public var currentFirstResponder: UIView? {
        guard !isFirstResponder else { return self }
        return subviews.compactMap({ return $0.currentFirstResponder }).first
    }
    
    public func roundCorners() {
        layer.cornerRadius = height/2
    }
}
