//
//  UIView+Fading.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

extension UIView {
    public func fadeIn() {
        alpha = 0.0
        UIView.animate(withDuration: 0.5) {
            self.alpha = 1.0
        }
    }
    
    public func fadeOut() {
        alpha = 1.0
        UIView.animate(withDuration: 0.5) {
            self.alpha = 0.0
        }
    }
}
