//
//  UIView+Screenshot.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension UIView {
    public func screenshot() -> UIImage? {
        var screenshotImage: UIImage?
        
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0);
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        layer.render(in: context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return screenshotImage
    }
}
