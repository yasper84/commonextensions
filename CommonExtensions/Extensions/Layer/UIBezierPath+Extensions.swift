//
//  UIBezierPath+Extensions.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

// MARK: Drawing extensions
public extension UIBezierPath {
    public func toShapeLayer(strokeColor: CGColor = UIColor.white.cgColor,
                      lineWidth: CGFloat = 1,
                      opacity: Float = 1) -> CAShapeLayer {
        let mask = CAShapeLayer()
        mask.fillColor = nil
        mask.strokeColor = strokeColor
        mask.path = cgPath
        mask.opacity = opacity
        mask.lineWidth = lineWidth
        return mask
    }
}
