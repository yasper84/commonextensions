//
//  Device+Extensions.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension UIDevice {
    private struct Constants {
        static let maxSmallSize: CGFloat = 320
    }
    
    public static var isLargeDevice: Bool {
        return UIScreen.main.bounds.size.width > Constants.maxSmallSize
    }
    
    public static var isSimulator: Bool {
        #if (arch(i386) || arch(x86_64))
        return true
        #else
        return false
        #endif
    }
    
    public static var modelIdentifier: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        return identifier
    }
    
    public static var phoneLanguage: String {
        return Locale.current.languageCode ?? "unknown"
    }
    
    public static var phoneRegion: String {
        return Locale.current.regionCode ?? "unknown"
    }
    
    public static func vibrate() {
        UIImpactFeedbackGenerator(style: UIImpactFeedbackGenerator.FeedbackStyle.heavy).impactOccurred()
    }
}
