//
//  Dictionary+KeyPath.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public struct Keypath {
    let path: String
    public init(_ path: String) {
        self.path = path
    }
}

// MARK: Keypath support
public extension Dictionary {
    public subscript(keyPath: Keypath) -> Any? {
        get {
            return valueForKeyPath(keyPath: keyPath.path)
        }
        set {
            setValue(value: newValue, forKeyPath: keyPath.path)
        }
    }
    
    mutating public func setValue(value: Any?, forKeyPath keyPath: String) {
        var keys = keyPath.components(separatedBy: ".")
        guard let first = keys.first as? Key else { print("Unable to use string as key on type: \(Key.self)"); return }
        keys.remove(at: 0)
        if keys.isEmpty, let settable = value as? Value {
            self[first] = settable
        } else {
            let rejoined = keys.joined(separator: ".")
            var subdict: [String : Any] = [:]
            if let sub = self[first] as? [String : Any] {
                subdict = sub
            }
            subdict.setValue(value: value, forKeyPath: rejoined)
            let settable = subdict as! Value
            self[first] = settable
        }
    }
    
    public func valueForKeyPath<T>(keyPath: String) -> T? {
        return (self as NSObject).value(forKeyPath: keyPath) as? T
    }
}
