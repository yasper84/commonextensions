//
//  Dictonary+DynamicAccess.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

// Allows for accessing dictionary-values directly: i.e. 'myDictionary.someVariable'
@dynamicMemberLookup
public protocol DictionaryDynamicLookup {
    associatedtype Key
    associatedtype Value
    subscript(key: Key) -> Value? { get }
}

public extension DictionaryDynamicLookup where Key == String {
    public subscript(dynamicMember member: String) -> Value? {
        return self[member]
    }
}

extension Dictionary: DictionaryDynamicLookup {}
