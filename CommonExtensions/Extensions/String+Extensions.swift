//
//  String+Extensions.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension String {
    public var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    public func isValidEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
    public func present(message: String? = nil,
                 actions: [(title: String, completion: (() -> Void))],
                 alertStyle: UIAlertController.Style = .alert,
                 navigationController: UINavigationController? = nil) {
        let alertController = UIAlertController(title: self,
                                                message: message,
                                                preferredStyle: alertStyle)
        
        for action in actions {
            alertController.addAction(UIAlertAction(title: action.title,
                                                    style: .default) { (selectedAction) in
                                                        action.completion()
            })
        }
        
        guard let navController = navigationController ?? UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else {
            return
        }
        navController.present(alertController,
                              animated: true,
                              completion: nil)
    }
}
