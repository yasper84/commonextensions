//
//  TimeInterval+Time.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 17/06/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension TimeInterval {
    public var toYears: Int {
        return Int(self/60/60/24/7/52)
    }
    
    public var toMonths: Int {
        return Int(self/60/60/24/7/30)
    }
    
    public var toWeeks: Int {
        return Int(self/60/60/24/7)
    }
    
    public var toDays: Int {
        return Int(self/60/60/24)
    }
    
    public var toHours: Int {
        return Int(self/60/60)
    }
    
    public var toMinutes: Int {
        return Int(self/60)
    }
}
